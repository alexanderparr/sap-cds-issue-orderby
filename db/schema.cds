namespace com.lenze.service.productidentification;

@cds.persistence.exists
entity MARA {
  key MATNR : String(18);
      NORMT : String(18);
      MTART : String(4);
      BRGEW : Double;
      NTGEW : Double;
      GEWEI : String(3);
      LAENG : Double;
      BREIT : Double;
      HOEHE : Double;
      MEABM : String(3);
      BISMT : String(18);
      MSTAE : String(2);
      MSTAV : String(2);
      LVORM : String(1);
      BEGRU : String(4);
};

// Classification View
entity Classification as
  select * from MARA
  inner join INOB
    on MARA.MATNR = INOB.OBJEK;

@cds.persistence.exists
entity INOB {
  key MANDT : String(3);
  key CUOBJ : String(18);
      OBJEK : String(50);
};
