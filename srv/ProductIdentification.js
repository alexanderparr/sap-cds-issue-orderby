const cds = require('@sap/cds')

module.exports = async function (){

    this.before ('READ','*', each => {

        // Workaround for issue in sap/cds-runtime when orderby object is an empty array, an invalid sql is generated.
        if(each.query.SELECT.orderBy && each.query.SELECT.orderBy.length === 0){
            //delete each.query.SELECT.orderBy;
            //console.log("OrderBy empty array removed")
        }

    })
  }