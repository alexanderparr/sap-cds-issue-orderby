using { com.lenze.service.productidentification as data } from '../db/schema';
service ProductIdentification{

  @readonly entity Maras as projection on data.MARA;
  
   @readonly
  entity Classification as select from data.Classification;
}