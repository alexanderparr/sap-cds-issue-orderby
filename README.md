# Create Tables
CREATE TABLE com_lenze_service_productidentification_INOB (
  MANDT NVARCHAR(3) NOT NULL,
  CUOBJ NVARCHAR(18) NOT NULL,
  OBJEK NVARCHAR(50),
  PRIMARY KEY(MANDT, CUOBJ)
);

CREATE TABLE com_lenze_service_productidentification_MARA (
  MATNR NVARCHAR(18) NOT NULL,
  NORMT NVARCHAR(18),
  MTART NVARCHAR(4),
  BRGEW DOUBLE,
  NTGEW DOUBLE,
  GEWEI NVARCHAR(3),
  LAENG DOUBLE,
  BREIT DOUBLE,
  HOEHE DOUBLE,
  MEABM NVARCHAR(3),
  BISMT NVARCHAR(18),
  MSTAE NVARCHAR(2),
  MSTAV NVARCHAR(2),
  LVORM NVARCHAR(1),
  BEGRU NVARCHAR(4),
  PRIMARY KEY(MATNR)
);